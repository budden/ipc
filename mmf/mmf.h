#define MMF_SEMAPHORE_TIMEOUT 20000
#define MMF_NO_OF_ITERATIONS 1000000
#define MMF_SEMAPHORE_CREATION_WAITING_SLEEP_MS 1000
#define MMF_LOG_STEP 10000
#define MMF_SEM_1_TO_2 "SsemMam1to2"
#define MMF_SEM_2_TO_1 "SsemMam2to1"


LPCTSTR pBuf;

typedef struct { 
	int allow_1_to_2;
	int allow_2_to_1;
	char load[100];
} StructuredMessage;

#define BUF_SIZE sizeof(StructuredMessage)

StructuredMessage msg;

#define FIELD_IN_PBUF(field_name) (((char *)pBuf) + ((char *)(&(msg.field_name)) - ((char *)(&msg))))

char garbage[100] = "012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678";

void quasisleep () {
	for (int i = 0; i < 10000; i++) {
		garbage[i % sizeof(garbage)] = garbage[(i + 1) % sizeof(garbage)]; }};

void acquire_sem_1_to_2(int timeout) {
	for (int i = 0; (timeout == 0) || (i < timeout); i++) { 
		CopyMemory(((PVOID)(&(msg.allow_1_to_2))), 
			(PVOID)FIELD_IN_PBUF(allow_1_to_2), 
			sizeof(msg.allow_1_to_2));
		if (msg.allow_1_to_2) break;
		quasisleep();
	}
	if (! msg.allow_1_to_2 ) {
		printf("failed to acquire sem_1_to_2\n");
		exit(1);
	}
	msg.allow_1_to_2 = 0;
	CopyMemory((PVOID)FIELD_IN_PBUF(allow_1_to_2), 
		((PVOID)(&(msg.allow_1_to_2))),
		sizeof(msg.allow_1_to_2));
}

void release_sem_1_to_2() {
	msg.allow_1_to_2 = 1;
	CopyMemory((PVOID)FIELD_IN_PBUF(allow_1_to_2), 
		((PVOID)(&(msg.allow_1_to_2))),
		sizeof(msg.allow_1_to_2));
}

void acquire_sem_2_to_1(int timeout) {
	//printf("%d, %d, %d\n", (char *)pBuf, FIELD_IN_PBUF(allow_1_to_2), FIELD_IN_PBUF(allow_2_to_1));
	//printf("%d, %d, %d, %d\n", (char *)(&msg), (char *)(&msg.allow_1_to_2), (char *)(&msg.allow_2_to_1), sizeof(msg.allow_2_to_1));
	for (int i = 0; (timeout == 0) || (i < timeout); i++) { 
		CopyMemory(((PVOID)(&(msg.allow_2_to_1))), 
			(PVOID)FIELD_IN_PBUF(allow_2_to_1), 
			sizeof(msg.allow_2_to_1));
		if (msg.allow_2_to_1) break;
		quasisleep();
	}
	if (! msg.allow_2_to_1 ) {
		printf("failed to acquire sem_2_to_1, %d\n", msg.allow_2_to_1);
		exit(1);
	}
	msg.allow_2_to_1 = 0;
	CopyMemory((PVOID)FIELD_IN_PBUF(allow_2_to_1),
		((PVOID)(&(msg.allow_2_to_1))), 
		sizeof(msg.allow_2_to_1));

}

void release_sem_2_to_1() {
	msg.allow_2_to_1 = 1;
	CopyMemory((PVOID)FIELD_IN_PBUF(allow_2_to_1),
		((PVOID)(&(msg.allow_2_to_1))), 
		sizeof(msg.allow_2_to_1));
}

// ��� �������� ��� ������, �������� � ������� ��������� ��������
void init_semaphores() {
	msg.allow_1_to_2 = 1;
	msg.allow_2_to_1 = 1;
	CopyMemory((PVOID)(pBuf), &msg, sizeof(StructuredMessage));
}
