// ������� - "������", ���� �� � ������ ������� ������ ����������� � ������ ��������. 
// https://docs.microsoft.com/en-us/windows/win32/memory/creating-named-shared-memory
// Removed \\Global to avoid having administrative privileges
// https://docs.microsoft.com/ru-ru/windows/win32/sync/using-semaphore-objects

#include "stdafx.h"


#include <windows.h>
#include <stdio.h>
#include <conio.h>
#include <tchar.h>
#include "../mmf.h"

TCHAR szName[]=TEXT("MyFileMappingObject");

int _tmain()
{
   HANDLE hMapFile;

   hMapFile = CreateFileMapping(
                 INVALID_HANDLE_VALUE,    // use paging file
                 NULL,                    // default security
                 PAGE_READWRITE,          // read/write access
                 0,                       // maximum object size (high-order DWORD)
                 BUF_SIZE,                // maximum object size (low-order DWORD)
                 szName);                 // name of mapping object
   if (hMapFile == NULL) {
      _tprintf(TEXT("Could not create file mapping object (%d).\n"),
             GetLastError());
      return 1;  }
   pBuf = (LPTSTR) MapViewOfFile(hMapFile,   // handle to map object
                        FILE_MAP_ALL_ACCESS, // read/write permission
                        0,
                        0,
                        BUF_SIZE);
   if (pBuf == NULL) {
      _tprintf(TEXT("Could not map view of file (%d).\n"),
             GetLastError());
       CloseHandle(hMapFile);
      return 1;  }

   init_semaphores();
   // ����������� �� ���� ����� �������, ������� �� ������ ����� �������, ����� ������� ������ ������
   acquire_sem_1_to_2(MMF_SEMAPHORE_TIMEOUT);
   Sleep(MMF_SEMAPHORE_CREATION_WAITING_SLEEP_MS);


   for (int i = 0; i < MMF_NO_OF_ITERATIONS; i++) {
	   int logThisStep = (i < 11) || (i % MMF_LOG_STEP) == 0;
		char* buf2 = msg.load;
        // �� ����� ������ � ���
		acquire_sem_2_to_1(MMF_SEMAPHORE_TIMEOUT);
		// ���, ���� ��� ��������� ������ 
		acquire_sem_1_to_2((i == 0) ? 0 : MMF_SEMAPHORE_TIMEOUT);

		if (logThisStep) sprintf_s(buf2,sizeof(msg.load),"Message: %d",i); // ����� ������ ������������ �������� � ���� ����, ��� ��� � ������� ����������
		if (logThisStep) { printf("mmf1: about to put to memory, count = %d\n",i); fflush(stdout); }


		// �����
		CopyMemory((PVOID)FIELD_IN_PBUF(load), &(msg.load), sizeof(msg.load));
		// �� �� �������� - �������, ��� ���� ���������� � ������
		release_sem_1_to_2();
		// ������ �� ���, ����� ��� ��������, ������� �������� ������ � ���� �������. ������ � ���� ������� ����� ������ �� �����, 
		// �� ����� ���� ��� ��� ��� �� ��������, ��� �������� ���� ������ - � ��� �� ������, ��� ����� ����� ��������� ������. 
		release_sem_2_to_1();
   }

   // ������ �� �����, ����� 1 ��� ���������
   // _getch();

   UnmapViewOfFile(pBuf);

   CloseHandle(hMapFile);
   printf("mmf1 DONE\n");
   return 0;
}