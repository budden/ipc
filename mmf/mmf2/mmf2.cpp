#include "stdafx.h"
#include <windows.h>
#include <stdio.h>
#include <conio.h>
#include <tchar.h>
#include "../mmf.h"

#pragma comment(lib, "user32.lib")

TCHAR szName[]=TEXT("MyFileMappingObject");


int _tmain()
{
   HANDLE hMapFile;

   hMapFile = OpenFileMapping(
                   FILE_MAP_ALL_ACCESS,   // read/write access
                   FALSE,                 // do not inherit the name
                   szName);               // name of mapping object
   if (hMapFile == NULL) {  _tprintf(TEXT("mmf2: Could not open file mapping object (%d).\n"), GetLastError()); exit(1); }

   pBuf = (LPTSTR) MapViewOfFile(hMapFile, // handle to map object
               FILE_MAP_ALL_ACCESS,  // read/write permission
               0,0,BUF_SIZE);
   if (pBuf == NULL) { _tprintf(TEXT("mmf2: Could not map view of file (%d).\n"),  GetLastError()); exit(1); }

   // ���, ���� ������ �������������� ��������
   // �� ����, ���� ���-�� ������������������, ����� ���������, ���� ��� ��������� ����� �������� � �������� ���� �������.
   // �� � ��� ������ ������ - ������ �������� ��������
   Sleep(MMF_SEMAPHORE_CREATION_WAITING_SLEEP_MS);

   // �� ������ � ������, ��������� �������, ����� ��� ����� �������� ���������
   release_sem_1_to_2();
   for (int i = 0; i < MMF_NO_OF_ITERATIONS; i++) {
	   int logThisStep = (i < 11) || (i % MMF_LOG_STEP) == 0;
        // ���, ���� ��� ������ ���������� � ������
	   acquire_sem_2_to_1((i == 0)? 0 : MMF_SEMAPHORE_TIMEOUT);

		// ��� �� �������� ������, �� ����� ������ �������� ��� ��� ����. 
	   acquire_sem_1_to_2(MMF_SEMAPHORE_TIMEOUT);

		if (logThisStep) printf("mmf2: about to start work (%d)\n",i);
		CopyMemory(((PVOID)(&msg.load)), FIELD_IN_PBUF(load), sizeof(msg.load));
		msg.load[sizeof(msg.load)-1] = 0;
		if (logThisStep) { printf("mmf2: got data = %s\n",msg.load,i); fflush(stdout); }

		// ����� �� ��� ����� ����������� �����, ���� �� ����� ���� ������ �� ������

		// ��������� ����� - ������, ��������� ����� �����, �� �������� ����������, ������� ��� ������ �� �����
		release_sem_2_to_1();
		release_sem_1_to_2();
   }

   // MessageBox(NULL, pBuf, TEXT("Process2"), MB_OK);

   // ������ �� ����� ����� 1 ��� ���������
   UnmapViewOfFile(pBuf); CloseHandle(hMapFile); 
   printf("mmf2 DONE\n");
   return 0;
}